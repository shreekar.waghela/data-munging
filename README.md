## Data Munging 

In this project we perform data munging on two files weather.dat and football.dat

From weather.dat file we extract data for the day with minimum temperature spread, i.e, minimum difference between maximum temperature and minimum temperature.

From football.dat file we extract data fot the team with minimum goal difference, i.e, minimum difference between for_goals and against_goals.
