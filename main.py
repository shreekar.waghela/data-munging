from data_extract import DataExtract
from data_transform import DataTransform
from data_munging import DataMunging

max_temp_index = 1
min_temp_index = 2
day_index = 0

team_index = 1
for_goal_index = 6 
against_goal_index = 8 

weather_munging = DataMunging(
    DataTransform(DataExtract('weather.dat').extract()).transform_data(max_temp_index, min_temp_index, day_index))
weather_munging.data_munging()

football_munging = DataMunging(
    DataTransform(DataExtract('football.dat').extract()).transform_data(for_goal_index, against_goal_index, team_index))
football_munging.data_munging()
