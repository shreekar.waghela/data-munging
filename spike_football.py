with open("football.dat") as football_file:
    football_data = {}
    for details in football_file:
        team_details = details.split()
        if len(team_details) > 1 :
            for_goals = team_details[-4]
            against_goals = team_details[-2]
            if for_goals.isnumeric() and against_goals.isnumeric():
                football_data[team_details[1]] = abs(int(for_goals) - int(against_goals))
        print(team_details)

for team, goal_diff in football_data.items():
    if football_data[team] == min(football_data.values()):
        print("team: {}, goal_diff: {}".format(team, goal_diff))
