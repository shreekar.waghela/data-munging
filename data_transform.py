from data_extract import DataExtract


class DataTransform(DataExtract):

    def __init__(self, data):
        self.data = data

    def transform_data(self, col1, col2, key):
        data_dict = {}
        for details in self.data:
            if len(details) > 8:
                upper = details[col1].replace('*', '')
                lower = details[col2].replace('*', '')
                if upper.isnumeric() and lower.isnumeric():
                    data_dict[details[key]] = abs(int(
                        upper) - int(lower))
        return data_dict

    def data_munging(self):
        pass
