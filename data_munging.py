from data_extract import DataExtract
from data_transform import DataTransform


class DataMunging(DataTransform):

    def __init__(self, data):
        self.data = data

    def data_munging(self):
        for key, diff in self.data.items():
            if self.data[key] == min(self.data.values()):
                if key.isnumeric():
                    print("day: {}, temperature: {}".format(key, diff))
                else:
                    print("team: {}, goal_diff: {}".format(key, diff))
