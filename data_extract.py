class DataExtract():

    def __init__(self, file_path):
        self.file_path = file_path

    def extract(self):
        with open(self.file_path, 'r') as weather_file:
            data = weather_file.readlines()
            data_array = []
            for rows in data:
                data_array.append(rows.strip().split())
        return data_array

    def transform_data(self):
        pass
