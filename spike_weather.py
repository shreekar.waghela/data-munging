with open("weather.dat") as weather_file:
    weather_data = {}
    for details in weather_file:
        daily_weather = details.split()
        if daily_weather != []:
            max_temp = daily_weather[1].replace('*', '')
            min_temp = daily_weather[2].replace('*', '')
            if max_temp.isnumeric() and min_temp.isnumeric():
                weather_data[daily_weather[0]] = int(max_temp) - int(min_temp)

for day, diff in weather_data.items():
    if weather_data[day] == min(weather_data.values()):
        print("day: {}, temp_diff: {}".format(day, diff))
